define(['text!user/favorite-blog-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'user-favorite-blog-list',
			events: {
				"click .backBtn": "goBack",
				"click .favoriteList": "goToBlogDetail",
			},
			goBack: function() {
				this.navigate("user-info", {
					trigger: true
				});
			},
			goToBlogDetail: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var fromType = 3;
				var checkDetail = "news/news-blog-detail";
				var com = 5;
				this.navigateModule("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.loadList(this, 'user-favorite-blog-list', OpenAPI.favorite_list, {
					'type': 3,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': access_token,
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});