define(['text!user/user-info.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'user-user-info',
			uploadImageURI: "",
			events: {
				"click .refreshBtn": "refresh",
				"click .backBtn": "goBack",
				"click #favorite": "goToFavorite",
				"click #attention": "goToAttention",
				"click #fans": "goToFans",
				"click .editUserpic": "editUserImg"
			},

			editUserImg: function() {
				var me = this;
				var dialog = new Piece.Dialog({
					autoshow: true,
					target: 'body',
					title: '请选择',
					content: ''
				}, {
					configs: [{
						title: "手机拍照",
						eventName: 'camera'
					}, {
						title: "手机相册",
						eventName: 'library'
					}],
					camera: function() {
						me.getPicture("CAMERA");
						// dialog.hide();
					},
					library: function() {
						me.getPicture("PHOTOLIBRARY");
						// dialog.hide();
					}
					
				});
				$('.cube-dialog-screen').click(function(){
					dialog.hide();
				})
			},

			getPicture: function(type) {
				var me = this;
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;

				var loader = new Piece.Loader({
					autoshow: false, //是否初始化时就弹出加载控件
					target: 'body' //页面目标组件表识
				});

				function win(r) {
					console.log("Code = " + r.responseCode);
					console.log("Response = " + r.response);
					console.log("Sent = " + r.bytesSent);
					var response = JSON.parse(r.response);
					if (response.error === "200") {
						new Piece.Toast('上传头像成功');
						$('.userpic').attr("src", me.uploadImageURI);
					} else {
						new Piece.Toast('上传失败,请重试');
					}
					loader.hide();
				};

				function fail(error) {
					loader.hide();
					new Piece.Toast('上传失败,请重试');
					// alert("An error has occurred: Code = " + error.code);
					console.log("upload error source " + error.source);
					console.log("upload error target " + error.target);
				};

				//上传图片
				function uploadPhoto(imageURI) {
					loader.show();
					var options = new FileUploadOptions();
					options.fileKey = "portrait";
					options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1) + ".jpg";
					options.mimeType = "image/jpeg";

					// options.chunkedMode = false;

					var params = {};
					params.access_token = access_token;

					options.params = params;

					var ft = new FileTransfer();
					ft.upload(imageURI, encodeURI(OpenAPI.portrait_update), win, fail, options);
				};


				var pictureSource = navigator.camera.PictureSourceType;
				var destinationType = navigator.camera.DestinationType;

				//选择图片完成后，马上上传。
				function onSuccess(imageUrl) {
					me.uploadImageURI = imageUrl;
					uploadPhoto(imageUrl);
				}
				//选择图片失败
				function onFail(message) {
					alert('Failed because: ' + message);
				}

				var source;
				if (type === "CAMERA") {
					source = pictureSource.CAMERA;
				} else {
					source = pictureSource.PHOTOLIBRARY;
				}

				navigator.camera.getPicture(onSuccess, onFail, {
					quality: 75,
					allowEdit: true,
					encodingType: Camera.EncodingType.JPEG,
					destinationType: destinationType.FILE_URI,
					sourceType: source
				});

			},

			refresh: function() {
				this.onShow();
			},
			goBack: function() {
				// history.back();
				var lastPage = Piece.Session.loadObject("osLastPage");
				this.navigateModule(lastPage, {
					trigger: true
				});
			},
			goToFavorite: function(el) {
				this.navigate("favorite-software-list", {
					trigger: true
				});
			},
			goToAttention: function(el) {
				var $target = $(el.currentTarget);
				var attentionNum = $target.attr("data-attentionNum");
				var fansNum = $target.attr("data-fansNum");
				this.navigate("attention-list?attentionNum=" + attentionNum + "&fansNum=" + fansNum, {
					trigger: true
				});
			},
			goToFans: function(el) {
				var $target = $(el.currentTarget);
				var attentionNum = $target.attr("data-attentionNum");
				var fansNum = $target.attr("data-fansNum");
				this.navigate("fans-list?fansNum=" + fansNum + "&attentionNum=" + attentionNum, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;

				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.Ajax(OpenAPI.my_information, "GET", {
					"access_token": access_token,
					"dataType": "jsonp"
				}, 'json', function(data, textStatus, jqXHR) {
					var userInfoTemplate = $(me.el).find("#userInfoTemplate").html();
					var userInfoHtml = _.template(userInfoTemplate, data);
					$(".content").html("");
					$(".content").append(userInfoHtml);
				}, null, null);
				//write your business logic here :)
			}
		}); //view define

	});